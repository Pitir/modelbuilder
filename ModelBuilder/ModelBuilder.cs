﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ModelBuilder
{
    public static class ModelBuilder
    {
        public static T FillModel<T>(T model)
        {
            var type = typeof(T);         

            return (T)FillModel(type);
        }

        public static object FillModel(Type type)
        {
            var constructor = GetConstructor(type);

            var obj = CreateObject(type, constructor);

            if (type.GetInterface(nameof(ICollection)) != null)
            {
                InvokeAdd(type, obj);
            }
            else if (!type.IsArray)
            {
                RecursiveCreateProperties(type, obj);
            }

            return obj;
        }

        private static void RecursiveCreateProperties(Type type, object obj)
        {
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                var propertyType = property.PropertyType;

                if (propertyType.IsClass)
                {
                    var child = FillModel(propertyType);

                    property.SetValue(obj, child);
                }
            }
        }

        private static void InvokeAdd(Type type, object obj)
        {
            var methodInfo = type.GetMethod("Add");

            if (methodInfo != null)
            {
                var objects = GetParameters(type, methodInfo.GetParameters());
                methodInfo.Invoke(obj, objects);
            }
        }

        private static ConstructorInfo GetConstructor(Type type)
        {
            return type
                .GetConstructors()
                .OrderBy(p => p.GetParameters().Length)
                .FirstOrDefault();
        }

        private static object CreateObject(Type type, ConstructorInfo constructor)
        {
            object obj = null;

            if (constructor != null)
            {
                var parameters = constructor.GetParameters();

                if (parameters != null && parameters.Any())
                {
                    var constructorParams = GetParameters(type, parameters);

                    obj = constructor.Invoke(constructorParams);
                }
                else
                {
                    obj = constructor.Invoke(new object[] { });
                }
            }

            return obj;
        }

        private static object[] GetParameters(Type type, ParameterInfo[] parameters)
        {
            var constructorParams = new List<object>();

            foreach (var parameter in parameters.Select(p => p.ParameterType))
            {
                var constructorParameter = parameter.IsClass ? FillModel(parameter) : GetDefault(parameter);

                if (type.IsArray)
                {
                    constructorParameter = 1;
                }

                constructorParams.Add(constructorParameter);
            }

            return constructorParams.ToArray();
        }

        private static object GetDefault(Type type)
        {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }
    }
}
