﻿using System.Collections;
using System.Collections.Generic;

namespace ModelBuilder.TestModels
{
    public class Class2
    {
        public int Int1 { get; set; }

        public ArrayList ArrayList { get; set; }

        public List<int> List { get; set; }
    }
}