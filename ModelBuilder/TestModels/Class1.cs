﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace ModelBuilder.TestModels
{
    public class Class1
    {
        public string String { get; set; }

        public DateTime Date { get; set; }

        public int Int { get; set; }

        public StringBuilder StringBuilder { get; set; }

        public Class2 Class2 { get; set; }

        public Dictionary<int, int> Dictionary { get; set; }

        public int[] ArrayInt { get; set; }

        public ConcurrentBag<Class2> ConcurrentBag { get; set; }

        public ArrayList ArrayList { get; set; }

        public List<List<Dictionary<string, Class2>>> ListListDictionary { get; set; }
    }
}
